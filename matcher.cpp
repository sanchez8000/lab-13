#include <iostream>
// #include "charstack.h"
#include "pairmatcher.h"

using namespace std;

// class CharStack {
// private:
//     char *stack;
//     int size = 0;
//     int maxSize;
// public:
//     // CharStack() : CharStack(100){} c++11 needed
//     CharStack(int max) {
//         maxSize = max;
//         stack = new char[maxSize];
//     }
//     ~CharStack() {
//         delete [] stack;
//     }
//     bool push(char c) {
//         if(size == maxSize){
//             return false;
//         }
//         stack[size++] = c;
//         return true;
//     }
//     char pop() {
//         if(isEmpty()){
//             return 0;
//         }
//         char retVal = stack[size - 1];
//         stack[size--];
//         return retVal;
//     }
//     bool isEmpty() const {
//         return size == 0;
//     }
// };

// class PairMatcher {
// private:
//     char _openChar, _closeChar;
//     CharStack charStack;
// public:
//     PairMatcher(char openChar, char closeChar) : charStack(100) {
//         _openChar = openChar;
//         _closeChar = closeChar;
//     }
//     bool check(const string &testString) {
//         for (int i = 0; i < testString.length(); i++) {
//             if (testString[i] == _openChar) {
//                 charStack.push(_openChar);
//             } else if (testString[i] == _closeChar) {
//                 if (charStack.pop() != _openChar) {
//                     return false;
//                 }
//             }
//         }
//         return charStack.isEmpty();
//     }
// };

int main() {
    PairMatcher matcher('(', ')');
    
    string testString = "((())()((())";
    
    cout << testString << " is " << (matcher.check(testString) ? "valid" : "invalid") << endl;
    return 0;
}
