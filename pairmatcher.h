#include "charstack.h"
#include <iostream>

class PairMatcher {
private:
    char _openChar, _closeChar;
    CharStack charStack;
public:
    PairMatcher(char openChar, char closeChar);
    bool check(const std::string &testString);
};