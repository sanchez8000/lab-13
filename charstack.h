class CharStack {
private:
    char *stack;
    int size = 0;
    int maxSize;
public:
    // CharStack() : CharStack(100){} c++11 needed
    CharStack(int max) {
        maxSize = max;
        stack = new char[maxSize];
    }
    ~CharStack() {
        delete [] stack;
    }
    bool push(char c) {
        if(size == maxSize){
            return false;
        }
        stack[size++] = c;
        return true;
    }
    char pop() {
        if(isEmpty()){
            return 0;
        }
        char retVal = stack[size - 1];
        stack[size--];
        return retVal;
    }
    bool isEmpty() const {
        return size == 0;
    }
};