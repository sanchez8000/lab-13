 #include "charstack.h"
 
    CharStack::CharStack(int max) {
        maxSize = max;
        stack = new char[maxSize];
    }
    CharStack::~CharStack() {
        delete [] stack;
    }
    CharStack::bool push(char c) {
        if(size == maxSize){
            return false;
        }
        stack[size++] = c;
        return true;
    }
    CharStack::char pop() {
        if(isEmpty()){
            return 0;
        }
        char retVal = stack[size - 1];
        stack[size--];
        return retVal;
    }
    CharStack::bool isEmpty() const {
        return size == 0;
    }